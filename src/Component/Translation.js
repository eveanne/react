import React from 'react'

const Translations = ({ translations }) => {

    return (
        <div>
            {translations.map((translation) => (
                <div class="translations">
                    <p className="">Translation FR : {translation.fr}</p>
                    

                </div>
            ))}
        </div>
    )
};

export default Translations;