
import React, { Component } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';


class MyMap extends Component {
  state = {
    lat: 33,
    lng: 65,
    zoom: 1,
  }

  render() {

    // const position = [this.props.coordonnees.latlng[0], this.props.coordonnees.latlng[1]]
    const position = [this.state.lat , this.state.lng]
    console.log(position)

    return (
      <Map className="map" center={position} zoom={this.state.zoom}>
        <TileLayer
          attribution='&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.thunderforest.com/pioneer/{z}/{x}/{y}.png?apikey={apikey}'
          apikey='0dd036afb1c449d9955309f0e0aa469c'
          // attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          // url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={position}>
          <Popup>
            A pretty CSS3 popup. <br /> Easily customizable.
          </Popup>
        </Marker>
      </Map>
    )
  }
};

export default MyMap

// L.tileLayer('https://{s}.tile.thunderforest.com/pioneer/{z}/{x}/{y}.png?apikey={apikey}', {
// 	attribution: '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'


// function MyMap({ coordonnees }) {

//   const position = [coordonnees.latlng[0], coordonnees.latlng[1]]
 