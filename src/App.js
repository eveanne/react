import React, { Component } from 'react';
import MyMap from './Component/MyMap';
// import Latlong from './Component/Latlong';
// import Translations from './Component/Translation';


class App extends Component {
  state = {
    query: "",
    data: [],
    filteredData: []
    };
  

  handleInputChange = event => {
    const query = event.target.value;

    this.setState(prevState => {
      const filteredData = prevState.data.filter(element => {
        return element.name.toLowerCase().includes(query.toLowerCase());
               
      });

      return {
        query,
        filteredData
      };
    });
  };

  getData = () => {
    fetch(`https://restcountries.eu/rest/v2/all`)
      .then(response => response.json())
      .then(data => {
        const { query } = this.state;
        const filteredData = data.filter(element => {
          return element.name.toLowerCase().includes(query.toLowerCase());
                  
        });

        this.setState({
          data,
          filteredData
        });
      });
  };

  componentWillMount() {
    this.getData();
  }
 

  render() {
    return (
      <div className="searchForm container">
            <form>
              <input
                placeholder="Search for..."
                value={this.state.query}
                onChange={this.handleInputChange}
              />
            </form>
              <div className='marginCard'>{this.state.filteredData.map(country => 
                <div className="card" key={country.id}>
                  <div className="card-body">
                    <h5 className="card-title">{country.name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">{country.nativeName}</h6>
                    <img className="flag" alt="flag" src={country.flag}/>
                    <p className="card-text">Capitale : {country.capital}</p> 
                    <p className="card-text">Region : {country.region}</p>
                    <p className="card-text">tr : {country.translations.fr}</p>
                    <p className="card-text">{country.latlng[0]}</p>
                    <p className="card-text">{country.latlng[1]}</p>
                  <div className="card">
                    <MyMap/>
                  </div> 
                  </div>
                </div>
            )}</div>
      </div>
    );
  }
  }


export default App;


 // positionCountry = () => {
  //   this.state.filteredData.map(country => {
  //     let position = [];
  //     position.push(country.latlng[0]);
  //     position.push(country.latlng[1]);
  //     return console.log(position);
  //   }
      
  //     );
  // }
